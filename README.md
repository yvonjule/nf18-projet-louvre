# NF18-Projet Louvre

## Titre du projet
Gestion du musée du Louvre

## Descripition
C'est un projet destiné à concevoir un SGBDR des œuvres et des expositions du musée du Louvre et à développer une interface d'application permettant l'interaction avec ce système.

## Membres du groupe
 Prenom|Nom|Email
:---:|:---:|:---:
Xia|QI|qi.xia@etu.utc.fr
Bastien|LE CALVE|bastien.le-calve@etu.utc.fr
Jules|YVON|jules.yvon@etu.utc.fr
Jinshan|GUO|jinshan.guo@etu.utc.fr

## Processus du projet
Etape|Date limite rendue|Etat
:---:|:---:|:---:
Rédaction de NDC|Avant *TD7*(Fait en *TD5*)|En cours
Créaton de MCD en PlantUML|Avant *TD6*|A commencer
Transformation en MLD|Avant *TD7*|A commencer
Instantiation SQL|Avant *TD8*|A commencer
Manipulation SQL|Avant *TD11*|A commencer
Interface & Application du Python|Avant *Semaine Finale*|A commencer



 
